#!/usr/bin/env bash

gallium -i root -t templates -o output -w &
python3 -m http.server --directory output
kill $!